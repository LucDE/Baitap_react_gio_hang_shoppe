import React, { Component } from "react";

export default class Cart extends Component {
  renderCart = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr
          key={item.name}
          className="bg-white border-b-2 text-dark border-pink-500"
        >
          <td
            scope="row"
            className="py-4 px-6 font-medium  text-center font-bold "
          >
            {item.name}
          </td>
          <td className="py-4 px-6 flex justify-center">
            <img style={{ width: 100 }} src={item.image} alt={item.name}></img>
          </td>
          <td className="py-4 px-6  text-center">
            <button
              className="p-2 px-3 hover:bg-pink-400 bg-amber-400  text-white rounded font-bold text-2xl"
              onClick={() => {
                this.props.handleChangeQuantity(item, -1);
              }}
            >
              -
            </button>
            <span className="mx-3">{item.itemPurchased}</span>
            <button
              className="p-2 hover:bg-pink-400 bg-amber-400  text-white rounded font-bold text-2xl"
              onClick={() => {
                this.props.handleChangeQuantity(item, 1);
              }}
            >
              +
            </button>
          </td>
          <td className="py-4 px-6 text-center">
            {item.price * item.itemPurchased}$
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="overflow-x-auto relative">
        <table className="w-full text-sm bg-pink-500 text-1xl">
          <thead className="text-xs text-white text-center uppercase bg-pink">
            <tr>
              <th scope="col" className="py-3 px-6">
                Product name
              </th>
              <th scope="col" className="py-3 px-6">
                Image
              </th>
              <th scope="col" className="py-3 px-6">
                Quantity
              </th>
              <th scope="col" className="py-3 px-6">
                Price
              </th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}
