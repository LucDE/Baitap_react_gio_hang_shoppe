import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { name, image } = this.props.product;
    return (
      <div className="p-2">
        <div className="flex flex-col items-center border-4 rounded-lg border-pink-600 p-3">
          <img src={image} alt={name}></img>
          <p className="font-medium">{name}</p>
          <button
            className=" bg-pink-600 hover:bg-amber-400 p-2 mt-3 rounded text-white"
            onClick={() => {
              this.props.handleAddToCart(this.props.product);
            }}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
