import Cart from "./Cart";
import ProductList from "./ProductList";
import products from "./data.json";
import React, { Component } from "react";

export default class ShoesStore extends Component {
  state = {
    data: products,
    cart: [],
  };
  handleAddToCart = (product) => {
    console.log("Add To cart");
    console.log("product: ", product);
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === product.id;
    });
    console.log("index: ", index);
    if (index === -1) {
      cloneCart.push({ ...product, itemPurchased: 1 });
    } else {
      cloneCart[index].itemPurchased++;
    }
    this.setState({ cart: cloneCart });
  };
  handleChangeQuantity = (product, value) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id === product.id;
    });
    cloneCart[index].itemPurchased += value;
    if (cloneCart[index].itemPurchased === 0) {
      cloneCart.splice(index, 1);
    }
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container py-5">
        <Cart
          cart={this.state.cart}
          handleChangeQuantity={this.handleChangeQuantity}
        />
        <ProductList
          data={this.state.data}
          handleAddToCart={this.handleAddToCart}
        />
      </div>
    );
  }
}
