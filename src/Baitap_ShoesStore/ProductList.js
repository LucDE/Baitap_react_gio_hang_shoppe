import ProductItem from "./ProductItem";
import React, { Component } from "react";

export default class ProductList extends Component {
  renderContent = () => {
    return this.props.data.map((product) => {
      return (
        <ProductItem
          key={product.name}
          product={product}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return (
      <div
        className=" w-3/4 grid grid-cols-4 gap-4"
        style={{ margin: "0 auto" }}
      >
        {this.renderContent()}
      </div>
    );
  }
}
